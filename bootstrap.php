<?php
define('ABSPATH', dirname(dirname(__FILE__)));
define('RMSPATH', ABSPATH.'/rms');
define('EXTPATH', ABSPATH.'/apps');

define('DEBUG', TRUE);
define('ENV',   'dev');

require_once(RMSPATH.'/loader.php');

require_once(RMSPATH.'/configure.php');

foreach (Flight::get('main.components') as $app) {
    foreach (array('models', 'backend', 'frontend') as $cmp) {
        $pth = EXTPATH.'/'.$app.'/'.$cmp.'.php';
        
        if (true or file_exists($pth)) {
            require_once($pth);
        }
    }
}

Flight::start();

