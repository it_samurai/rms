<?php
class RMS {
    public static function backend ($key, $pattern, $callback) {
        Flight::route("/admin/$key{$pattern}", Context::wrap($key, 'backend', $callback));
    }
    
    public static function frontend ($key, $pattern, $callback) {
        Flight::route($pattern, Context::wrap($key, 'frontend', $callback));
    }
    
    private static $breads = array();
    
    public static function breadcrumbs ($key, $target, $title, $mapping) {
        if (!array_key_exists($key, RMS::$breads)) {
            RMS::$breads[$key] = array(
                'title'  => $title,
                'target' => $target,
                'links'  => $mapping,
            );
        }
    }
    
    public static function request_context ($cnt) {
        $resp = array();
        
        if ($cnt!=null) {
            $rst = array();
            
            if (array_key_exists($cnt->app, RMS::$breads)) {
                $brd = RMS::$breads[$cnt->app];
                
                $resp['breadcrumb'] = array(
                    'title'   => $brd['title'],
                    'url'     => $cnt->urlize($brd['target']),
                    'entries' => array(),
                );
                
                foreach ($brd['links'] as $lnk => $cb) {
                    $url = $cnt->urlize($lnk);
                    
                    $params = array();
                    
                    $resp['breadcrumb']['entries'][] = array(
                        'title'   => $cb($params),
                        'link'    => $url,
                        'enabled' => (Flight::request()->url==$url),
                    );
                }
            }
        }
        
        return $resp;
    }
    
    public static function mail ($alias, $reply_to=array()) {
        $mail = new PHPMailer();
        
        $mail->IsSMTP();
        $mail->Host = 'smtp.gmail.com';
        $mail->Port = 587;
        $mail->SMTPAuth = true;
        $mail->SMTPSecure = 'tls';
        
        $mail->From = 'tazekkaparchotel@gmail.com';
        $mail->FromName = 'Tazekka Parc Hotel :: Website';
        
        //$mail->AddReplyTo($reply_to);
        
        return $mail;
    }
}

