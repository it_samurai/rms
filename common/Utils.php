<?php
class Context {
    public static function wrap ($key, $ns, $cb) {
        $resp = new Context($key, $ns, $cb);
        
        return $resp;
    }
    
    public $app;
    public $page;
    
    private $ns;
    private $cb;
    
    private function __construct ($key, $ns, $cb) {
        $this->ns  = $ns;
        $this->cb  = $cb;
        
        $this->app  = $key;
        $this->page = new WebPage($this);
        
        $this->context = array(
            'cnt'  => $this,
            'page' => $this->page,
        );
    }
    
    public function __invoke () {
        $hnd = $this->cb;
        
        return $hnd($this);
    }
    
    public function urlize ($lnk) {
        $resp = $lnk;
        
        if ($this->app) {
            $resp = $this->app.$lnk;
        }
        
        if ($this->ns=='backend') {
            $resp = "/admin/{$resp}";
        }
        
        return $resp;
    }
    
    public function render ($pth, $args=array()) {
        
        $resp = array();
        
        if ($this->app) {
            $resp[] = EXTPATH.'/'.$this->app.'/'.$this->ns.'/'.$pth.'.php';
        }
        
        if ($this->ns=='backend') {
            $resp[] = RMSPATH.'/templates/'.$pth.'.php';
        } else if ($this->ns=='frontend') {
            $resp[] = ABSPATH.'/templates/'.$pth.'.php';
        }
        
        $env = array_merge($this->context, array());
        
        if (is_array($args)) {
            $env = array_merge($env, $args);
        }
        
        $env = array_merge($env, RMS::request_context($this));
        
        foreach ($resp as $tpl) {
            if (file_exists($tpl)) {
                return $this->render_php($tpl, $env);
            }
        }
        
        throw new \Exception("Template file not found at : <ul><li>".implode("</li><li>", $resp)."</li></ul>");
    }
    private function render_php($tpl, $env) {
        extract($env);
        
        unset($env);
        
        include $tpl;
        
        return null;
    }
}

