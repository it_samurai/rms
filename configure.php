<?php
$cfg = null;

foreach (array(
    //'.yml'  => function ($pth) { return yaml_parse_file($pth); },
    '.json' => function ($pth) { return json_decode(utf8_encode(file_get_contents($pth)), true); },
) as $ext => $cb) {
    $pth = ABSPATH.'/vhost'.$ext;
    
    if (true or file_exists($pth)) {
        try {
            $cfg = $cb($pth);
            
            break;
        } catch (Exception $ex) {
            $cfg = null;
        }
    }
}

if (false) {
    $cfg = array(
        'i18n' => array(
            'lang' => 'fr',
            'support' => array('fr', 'en', 'es'),
        ),
        'main' => array(
            'components' => array('pages', 'gallery', 'rooms'),
        ),
        'ganalytics' => array(
            'identifier' => 'UA-43065869-1',
        ),
        'db' => array(
            'dev' => array(
                'type' => 'mysql',
                'host' => 'us-cdbr-east-04.cleardb.com',
                'user' => 'b4a25528e03c84',
                'pass' => 'ace13372',
                'name' => 'heroku_b682205b9fa5507',
            ),
        ),
    );
}

foreach ($cfg as $k => $s) {
    foreach ($s as $l => $v) {
        //echo "{$k}.{$l}:\t\t\t"; print_r($v); echo "\n\n";
        
        Flight::set($k.'.'.$l, $v);
    }
}

//print_r($cfg);
//die();

Flight::set('debug', DEBUG);

Flight::set('flight.log_errors', DEBUG);
Flight::set('flight.views.path', ABSPATH.'/templates');

if (array_key_exists(ENV, $cfg['db'])) {
    $db = $cfg['db'][ENV];
    
    R::setup("{$db['type']}:host={$db['host']}; dbname={$db['name']}",$db['user'],$db['pass']);
}

$lang = Flight::get('i18n.lang');

foreach (array(
    function ($k) { return Flight::request()->query->$k; },
    function ($k) { return Flight::request()->cookies->$k; },
) as $cb) {
    $v = $cb('i18n_lang');
    
    if ($lang==null and $v!='') {
        $lang = $v;
    }
    
    if (DEBUG) {
        //print "{$lang}\n{$v}\n\n";
    }
}

Flight::response()->cookies['i18n_lang'] = $lang;

Flight::set('i18n.lang', $lang);

