<?php
require_once(RMSPATH.'/shortcuts.php');

$paths = array(
    '3rd' => array(
        'flight/flight/Flight',
        'redbean/RedBean/redbean.inc',
        'mailer/class.phpmailer',
    ),
    'common' => array('Utils', 'Page', 'Core', 'HTML5', 'Form'),
);

foreach ($paths as $prefix => $files) {
    foreach ($files as $pth) {
        require_once(RMSPATH.'/'.$prefix.'/'.$pth.'.php');
    }
}

