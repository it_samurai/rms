<?php
function renderTpl ($tpl, $args) {
    Flight::render('inc/header');
    Flight::render($tpl, $args);
    Flight::render('inc/footer');
}

function renderPage ($tpl, $args) {
    renderTpl("pages/{$tpl}", $args);
}

function T($key) {
    $lang = Flight::get('lang');
    
    return $key;
}

