<div>
    <ul class="breadcrumb">
        <li>
            <a href="<?php echo $breadcrumb['url'] ?>">
                <?php echo $breadcrumb['title'] ?>
            </a>
            <span class="divider">/</span>
        </li>
<?php
    foreach ($breadcrumb['entries'] as $entry) {
        if ($entry['enabled']) {
?>
        <li class="active"><?php echo $entry['title'] ?></li>
<?php
        }
    }
    /*
        <li>
            <a href="<?php echo $entry['link'] ?>">
                <?php echo $entry['title'] ?>
            </a>
            <span class="divider">/</span>
        </li>
    */
?>
    </ul>
</div>

