  <footer>
    <p class="pull-right">&copy; Simplenso 2012</p>
  </footer>
    <div id="box-config-modal" class="modal hide fade in" style="display: none;">
      <div class="modal-header">
        <button class="close" data-dismiss="modal">×</button>
        <h3>Adjust widget</h3>
      </div>
      <div class="modal-body">
        <p>This part can be customized to set box content specifix settings!</p>
      </div>
      <div class="modal-footer">
        <a href="#" class="btn btn-primary" data-dismiss="modal">Save Changes</a>
        <a href="#" class="btn" data-dismiss="modal">Cancel</a>
      </div>
    </div>
</div><!--/.fluid-container-->
        <script type="text/javascript" src="http://www.google.com/jsapi"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
        <script src="/cdn/admin/scripts/DataTables/media/js/jquery.dataTables.js"></script>
        <script src="/cdn/admin/scripts/jquery-ui/ui/minified/jquery.ui.core.min.js"></script>
        <script src="/cdn/admin/scripts/jquery-ui/ui/minified/jquery.ui.widget.min.js"></script>
        <script src="/cdn/admin/scripts/jquery-ui/ui/minified/jquery.ui.mouse.min.js"></script>
        <script src="/cdn/admin/scripts/jquery-ui/ui/minified/jquery.ui.sortable.min.js"></script>
        <script src="/cdn/admin/scripts/jquery-ui/ui/minified/jquery.ui.widget.min.js"></script>
        <script src="/cdn/admin/scripts/jquery-ui/ui/minified/jquery.ui.draggable.min.js"></script>
        <script src="/cdn/admin/scripts/jquery-ui/ui/minified/jquery.ui.droppable.min.js"></script>
        <script src="/cdn/admin/bootstrap/js/bootstrap.min.js"></script>
        <script src="/cdn/admin/scripts/bootbox/bootbox.min.js"></script>
        <script src="/cdn/admin/scripts/datepicker/js/bootstrap-datepicker.js"></script>
        <script src="/cdn/admin/scripts/jquery.cookie/jquery.cookie.js"></script>
        <script type='text/javascript' src='/cdn/admin/scripts/fullcalendar/fullcalendar/fullcalendar.min.js'></script>
        <script type="text/javascript" src="/cdn/admin/scripts/ckeditor/ckeditor.js"></script>
        <script type="text/javascript" language="javascript" src="/cdn/admin/scripts/chosen/chosen/chosen.jquery.min.js"></script>
        <script type="text/javascript" language="javascript" src="/cdn/admin/scripts/uniform/jquery.uniform.min.js"></script>

        <script src="http://blueimp.github.com/JavaScript-Templates/tmpl.min.js"></script>
        <script src="http://blueimp.github.com/JavaScript-Load-Image/load-image.min.js"></script>
        <script src="http://blueimp.github.com/JavaScript-Canvas-to-Blob/canvas-to-blob.min.js"></script>
        <script src="http://blueimp.github.com/Bootstrap-Image-Gallery/js/bootstrap-image-gallery.min.js"></script>
        <script src="/cdn/admin/scripts/blueimp-jQuery-File-Upload/js/jquery.iframe-transport.js"></script>
        <script src="/cdn/admin/scripts/blueimp-jQuery-File-Upload/js/jquery.fileupload.js"></script>
        <script src="/cdn/admin/scripts/blueimp-jQuery-File-Upload/js/jquery.fileupload-ip.js"></script>
        <script src="/cdn/admin/scripts/blueimp-jQuery-File-Upload/js/jquery.fileupload-ui.js"></script>
        <script src="/cdn/admin/scripts/blueimp-jQuery-File-Upload/js/main.js"></script>
        <!--[if gte IE 8]><script src="/cdn/admin/scripts/blueimp-jQuery-File-Upload/js/cors/jquery.xdr-transport.js"></script><![endif]-->
        <script src="/cdn/admin/scripts/simplenso/simplenso.js"></script>
    </body>
</html>

